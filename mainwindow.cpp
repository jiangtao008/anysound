#include "mainwindow.h"

MainWidget::MainWidget(QWidget *parent) : QWidget(parent)
{
    initialize();
    setupUI();

    audio = new QAudioOutput(QAudioDeviceInfo::defaultOutputDevice(),defaultFormat());
    source = new sound(fqSlider->value(),volSlider->value());

    source->open(QIODevice::ReadWrite);

    connect(playBtn,SIGNAL(clicked()),this,SLOT(slotPlay()));
    connect(stopBtn,SIGNAL(clicked()),this,SLOT(slotStop()));

    connect(fqSlider,SIGNAL(valueChanged(int)),this,SLOT(slotFqChanged(int)));
    connect(volSlider,SIGNAL(valueChanged(int)),this,SLOT(slotVolChanged(int)));
}

void MainWidget::slotFqChanged(int t){
    lb1->setText(QString("频率:%1").arg(t));
    source->setFreq(t);
}

void MainWidget::slotVolChanged(int t){
    lb2->setText(QString("音量:%1").arg(t));
    source->setVolume(t);
}

void MainWidget::slotPlay(){
    audio->start(source);
}

void MainWidget::slotStop(){
    audio->stop();
}

void MainWidget::initialize(){
    this->setFixedSize(400,240);
    fqSlider=new QSlider(Qt::Horizontal);
    fqSlider->setRange(0,15000);
    fqSlider->setValue(1000);

    volSlider=new QSlider(Qt::Horizontal);
    volSlider->setRange(0,100);
    volSlider->setValue(100);

    lb1=new QLabel("频率:1000");
    lb2=new QLabel("音量:100");

    lb1->setFixedWidth(120);
    lb2->setFixedWidth(120);

    playBtn=new QPushButton;
    stopBtn=new QPushButton;
    playBtn->setText("play");
    stopBtn->setText("stop");


    lay1=new QHBoxLayout;
    lay1->addWidget(lb1);
    lay1->addWidget(fqSlider);
    lay2=new QHBoxLayout;
    lay2->addWidget(lb2);
    lay2->addWidget(volSlider);


    lay3=new QHBoxLayout;
    lay3->addStretch();
    lay3->addWidget(playBtn);
    lay3->addStretch();
    lay3->addWidget(stopBtn);
    lay3->addStretch();


    mainLay=new QVBoxLayout;
    mainLay->addLayout(lay1);
    mainLay->addLayout(lay2);
    mainLay->addLayout(lay3);

    this->setLayout(mainLay);
}

void MainWidget::setupUI(){

}

