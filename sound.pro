#-------------------------------------------------
#
# Project created by QtCreator 2020-02-02T12:41:16
#
#-------------------------------------------------

QT       += core gui  multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = anySound
TEMPLATE = app

MOC_DIR = temp/moc
RCC_DIR = temp/rcc
UI_DIR = temp/ui
OBJECTS_DIR = temp/obj

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
        mainwindow.cpp \
        sound.cpp

HEADERS += \
        mainwindow.h \
        sound.h

FORMS += \
        mainwindow.ui
