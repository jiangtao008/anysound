#ifndef SOUND_H
#define SOUND_H

#include <QIODevice>
#include <QObject>

class sound : public QIODevice
{
public:
    sound();
    sound(int _fq, int _vol);

    void setFreq(int fq);
    void setVolume(int vol);

protected:
    qint64 readData(char *data, qint64 maxlen);
    qint64 writeData(const char *data, qint64 len);
private:
    double frequency=1000;
    double volume=100;

    qint64 it=0;

    QList<char> m_data;
    void appendData();
};

#endif // SOUND_H
