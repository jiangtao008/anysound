#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QAudioFormat>
#include <QAudioOutput>
#include <QSlider>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include "sound.h"

class MainWidget : public QWidget
{
    Q_OBJECT
    void initialize();
    void setupUI();

    QSlider *fqSlider,*volSlider;
    QLabel *lb1,*lb2;
    QPushButton *playBtn,*stopBtn;

    QAudioOutput *audio;
    sound *source;

    QHBoxLayout *lay1, *lay2, *lay3;
    QVBoxLayout *mainLay;

    QAudioFormat defaultFormat()
    {
        QAudioFormat audioFormat;
        audioFormat.setCodec("audio/pcm");
        audioFormat.setByteOrder(QAudioFormat::LittleEndian);
        audioFormat.setSampleRate(44100);
        audioFormat.setChannelCount(2);
        audioFormat.setSampleSize(16);
        audioFormat.setSampleType(QAudioFormat::SignedInt);
        return audioFormat;
    }
public:
    explicit MainWidget(QWidget *parent = nullptr);

signals:

public slots:
    void slotPlay();
    void slotStop();
    void slotFqChanged(int t);
    void slotVolChanged(int t);
};

#endif // MAINWIDGET_H
